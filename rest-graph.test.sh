#! /bin/bash

echo "Check USDT Pools..."
curl -s 'http://localhost:8080/api/v1/token/0xdac17f958d2ee523a2206206994597c13d831ec7/pools' |jq .

echo "Check USDT Swapped Volume in Time Range..."
curl -s 'http://localhost:8080/api/v1/token/0xdac17f958d2ee523a2206206994597c13d831ec7/swappedVolumeTimeRange?timeStart=1625105709&timeEnd=1625105721' |jq .

echo "Check Swaps in Block..."
curl -s 'http://localhost:8080/api/v1/block/13046554/swaps' |jq .

echo "Check Swaped Tokens in Block..."
curl -s 'http://localhost:8080/api/v1/block/13046554/swappedTokens' |jq .