# rest-graph

[![pipeline status](https://gitlab.com/0xJungleGorilla/rest-graph/badges/main/pipeline.svg)](https://gitlab.com/0xJungleGorilla/rest-graph/-/commits/main)

A rest based frontend to query Uniswap using [TheGraph](https://thegraph.com/) to get specific information about given assets and blocks.

This project is the result of a coding challenge with the following goals:

- Given an asset ID:
  - What pools exist that include it?
  - What is the total volume of that asset swapped in a given time range?
- BONUS! Given a block number:
  - What swaps occurred during that specific block?
  - List all assets swapped during that specific block

In addition, some minutiae:

- At the moment, The Graph APIs are free and require no authentication; don’t worry about auth
- You should respond to requests in JSON, or with proper status codes
- All monetary amounts should be in US dollar terms
- All time input and output should be Unix timestamps

## Install

Download and run the latest `rest-graph` binary for your OS

| OS      | Architecture | Download                                                                                                                                                   |
| ------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Linux   | AMD64        | [rest-graph](https://gitlab.com/0xJungleGorilla/rest-graph/-/jobs/artifacts/main/raw/bin/linux-amd64/rest-graph?job=build%3A+%5Bamd64%2C+linux%5D)         |
| Linux   | 386          | [rest-graph](https://gitlab.com/0xJungleGorilla/rest-graph/-/jobs/artifacts/main/raw/bin/linux-386/rest-graph?job=build%3A+%5B386%2C+linux%5D)             |
| Windows | AMD64        | [rest-graph](https://gitlab.com/0xJungleGorilla/rest-graph/-/jobs/artifacts/main/raw/bin/windows-amd64/rest-graph.exe?job=build%3A+%5Bamd64%2C+windows%5D) |
| Windows | 386          | [rest-graph](https://gitlab.com/0xJungleGorilla/rest-graph/-/jobs/artifacts/main/raw/bin/windows-386/rest-graph.exe?job=build%3A+%5B386%2C+windows%5D)     |
| Darwin  | AMD64        | [rest-graph](https://gitlab.com/0xJungleGorilla/rest-graph/-/jobs/artifacts/main/raw/bin/darwin-amd64/rest-graph?job=build%3A+%5Bamd64%2C+darwin%5D)       |

## Usage

Download the `rest-graph` web server and then query via REST calls to `http://localhost:8080`.

> NOTE: If you would like to configure the rest-graph server to listen on a different address/port, create a `rest-graph.config` file with the desired setting.
>
> ```bash
>    echo 'LISTEN_ADDRESS=0.0.0.0:8080' > rest-graph.config
> ```

## Features & Usage Examples

1. What pools exist for a Token?

- URL: `/api/v1/token/{TokenID}/pools`
- URL PARAM: `{TokenID}` is the Ethereum contract address for the token you would like to lookup.

Example Call:

- `TokenID`=0xdac17f958d2ee523a2206206994597c13d831ec7
  > USDT Ethereum Contract is 0xdac17f958d2ee523a2206206994597c13d831ec7

```bash
curl -s 'http://localhost:8080/api/v1/token/0xdac17f958d2ee523a2206206994597c13d831ec7/pools'
```

Example Output:

```json
{
  "token": {
    "id": "0xdac17f958d2ee523a2206206994597c13d831ec7",
    "name": "Tether USD",
    "symbol": "USDT",
    "whitelistPools": [
      {
        "id": "0x0cbe2f86e2fd90040ebb557b99f83400bf8f3717",
        "token0": {
          "name": "Maker",
          "symbol": "MKR"
        },
        "token1": {
          "name": "Tether USD",
          "symbol": "USDT"
        }
      },
      {
        "id": "0x11b815efb8f581194ae79006d24e0d814b7697f6",
        "token0": {
          "name": "Wrapped Ether",
          "symbol": "WETH"
        },
        "token1": {
          "name": "Tether USD",
          "symbol": "USDT"
        }
      }
    ]
  }
}
```

2. What is the total volume (in USD) of a token swapped in a given time range?

- URL: `/api/v1/token/{TokenID}/swappedVolumeTimeRange`
- URL PARAM: `{TokenID}` is the Ethereum contract address for the token you would like get the total volume of.
- Query String PARAMS:
  - `timeStart`: Beginning of time range to search (Unix timestamp format)
  - `timeEnd`: End of time range to search (Unix timestamp format)

Example Call:

- `TokenID`=0xdac17f958d2ee523a2206206994597c13d831ec7
  > USDT Ethereum Contract is 0xdac17f958d2ee523a2206206994597c13d831ec7
- `timeStart`=1625105709
  > 1625105709=Thu, 01 Jul 2021 02:15:09 GMT
- `timeEnd`=1625105721
  > 1625105721=Thu, 01 Jul 2021 02:15:21 GMT

```bash
curl -s 'http://localhost:8080/api/v1/token/0xdac17f958d2ee523a2206206994597c13d831ec7/swappedVolumeTimeRange?timeStart=1625105709&timeEnd=1625105721'
```

Example Output:

```json
{
  "id": "0xdac17f958d2ee523a2206206994597c13d831ec7",
  "name": "Tether USD",
  "symbol": "USDT",
  "timeStart": "1625105709",
  "timeEnd": "1625105721",
  "totalVolumeUSD": "892929.641840142547152936458587646484375"
}
```

3. What swaps have occured during a specific block?

- URL: `/api/v1/block/{BlockID}/swappedTokens`
- URL PARAM: `{BlockID}` is the block number you would like to check for swaps in.

Example Call:

- `BlockID`=13046554
  > USDT Ethereum Contract is 0xdac17f958d2ee523a2206206994597c13d831ec7

```bash
curl -s 'http://localhost:8080/api/v1/block/13046554/swaps
```

Example Output:

```json
{
  "swaps": [
    {
      "id": "0x000007762b32364a5e1f49e2f304f340b732414c186b87925178d5f064bcb0db#66038"
    },
    {
      "id": "0x00015408341f8846f316655bd7dbbe0a13a528b1388ed5809ce13de607675058#91637"
    }
  ]
}
```

4. What tokens have been swapped during a specific block?

- URL: `/api/v1/block/{BlockID}/swaps`
- URL PARAM: `{BlockID}` is the block number you would like to check what tokens have been swapped in.

Example Call:

- `BlockID`=13046554

```bash
curl -s 'http://localhost:8080/api/v1/block/13046554/swappedTokens'
```

Example Output:

```json
{
  "blockID": "13046554",
  "swappedTokenIDs": [
    "0x2260fac5e5542a773aa44fbcfedf7c193bc2c599",
    "0x0f5d2fb29fb7d3cfee444a200298f468908cc942"
  ]
}
```

## Roadmap

1. Fix security vulnerabilities of unhandled exceptions found in the [Security Vulnerability Report](https://gitlab.com/0xJungleGorilla/rest-graph/-/security/vulnerability_report)
2. Determine if json data output structures should be enhanced or confirm if passthrough of TheGraph.com's formatting is preferred to optimize performance.
3. Unit tests
4. Performance improvements

## Contributing Guide

Use Visual Studio Code for development. Dependencies have been baked into the Dev Container image and settings file. If developing outside of the recommended environment, please ensure pre-commit is installed.

## Support

Support is available for a fixed price of 0.5 BTC per minute. Fiat not accepted :)

## Project status

This project author is in need of a new career in the crypto industry :)
