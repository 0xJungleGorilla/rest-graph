module gitlab.com/0xJungleGorilla/rest-graph

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
